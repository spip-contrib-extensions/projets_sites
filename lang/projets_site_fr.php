<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_projets_site' => 'Ajouter ce site',
	'apache_fieldset_label' => 'Apache',
	'auteur_email_label' => 'E-mail',
	'auteur_id_label' => '#',
	'auteur_login_label' => 'Login',
	'auteur_nom_label' => 'Nom',
	'auteurs_admin_fieldset_label' => 'Administrateurs',
	'auteurs_explication' => 'Saisir la liste des auteurs du site en respectant les règles suivantes&nbsp;: </p><ul class="spip"><li>Un administrateur par ligne ;</li><li>L’identifiant, le login, l’email et le nom seront séparés par un pipe "|" ;</li><li>Pas de raccourcis typographiques de SPIP, ni de code html.</li></ul>',
	'auteurs_fieldset_label' => 'Les auteurs',
	'auteurs_titre_explication' => '<h4>Règles de saisies</h4>',
	'auteurs_webmestres_fieldset_label' => 'Webmestres',
	'autres_fieldset_label' => 'Autres',
	'avis_site_introuvable' => 'Site introuvable',

	// B
	'bo_fieldset_label' => 'Back office',
	'bo_url_format' => 'L’url du Back Office doit impérativement commencer par <em>http://</em> ou <em>https://</em>',

	// C
	'champ_apache_modules_label' => 'Modules Apache&nbsp;:',
	'champ_auteurs_admin_label' => 'Administrateurs&nbsp;:',
	'champ_auteurs_webmestres_label' => 'Webmestres&nbsp;:',
	'champ_autres_outils_label' => 'Autres outils&nbsp;:',
	'champ_bo_login_label' => 'Identifiant du Back Office&nbsp;:',
	'champ_bo_password_label' => 'Mot de passe du Back Office&nbsp;:',
	'champ_bo_url_label' => 'Url du Back Office&nbsp;:',
	'champ_date_creation_label' => 'Date de création&nbsp;:',
	'champ_descriptif_label' => 'Descriptif',
	'champ_fo_login_label' => 'Identifiant du Front Office&nbsp;:',
	'champ_fo_password_label' => 'Mot de passe du Front Office&nbsp;:',
	'champ_fo_url_label' => 'Url du Front Office&nbsp;:',
	'champ_id_projets_site_label' => 'Identifiant du site',
	'champ_logiciel_charset_label' => 'Jeu de caractères&nbsp;:',
	'champ_logiciel_nom_label' => 'Nom du logiciel&nbsp;:',
	'champ_logiciel_plugins_explication' => 'Saisir la liste des plugins du site en respectant les règles suivantes&nbsp;: </p><ul class="spip"><li>Un plugin par ligne ;</li><li>Le préfixe, le numéro de version, le titre complet et le statut du plugin seront séparés par un pipe "|" ;</li><li>Pas de raccourcis typographiques de SPIP, ni de code html.</li></ul>',
	'champ_logiciel_plugins_label' => 'Plugins du logiciel&nbsp;:',
	'champ_logiciel_revision_label' => 'Numéro de révision&nbsp;:',
	'champ_logiciel_version_format' => 'Vous devez saisir un numéro de version sous la forme "x.y.z", "x.y" ou "x".',
	'champ_logiciel_version_label' => 'Version du logiciel&nbsp;:',
	'champ_maj_label' => 'Date de mise à jour',
	'champ_moteur_recherche_explication' => 'Exemples&nbsp;: Moteur interne au logiciel, Exalead, Google, etc.',
	'champ_moteur_recherche_label' => 'Moteur de recherche&nbsp;:',
	'champ_perimetre_acces_label' => 'Périmètre d’accès&nbsp;:',
	'champ_php_extensions_label' => 'Extensions PHP&nbsp;:',
	'champ_php_memory_label' => 'Mémoire allouée à PHP&nbsp;:',
	'champ_php_timezone_label' => 'Fuseau horaire&nbsp;:',
	'champ_php_version_label' => 'Version de PHP&nbsp;:',
	'champ_remarques_explication' => 'Caractéristiques particulières, fonctionnalités, etc.',
	'champ_remarques_label' => 'Remarques&nbsp;:',
	'champ_sas_login_label' => 'Identifiant&nbsp;:',
	'champ_sas_password_label' => 'Mot de passe&nbsp;:',
	'champ_sas_port_label' => 'Port&nbsp;:',
	'champ_sas_protocole_explication' => 'Exemples&nbsp;: FTP, SFTP, etc.',
	'champ_sas_protocole_label' => 'Protocole de connexion&nbsp;:',
	'champ_sas_serveur_label' => 'Serveur SAS&nbsp;:',
	'champ_serveur_logiciel_label' => 'Logiciel du serveur&nbsp;:',
	'champ_serveur_nom_label' => 'Nom du serveur&nbsp;:',
	'champ_serveur_path_label' => 'Chemin du site sur le serveur&nbsp;:',
	'champ_serveur_port_label' => 'Port du serveur&nbsp;:',
	'champ_serveur_surveillance_label' => 'Surveillance du serveur&nbsp;:',
	'champ_sgbd_charset_label' => 'Jeu de caractères&nbsp;:',
	'champ_sgbd_collation_label' => 'Collation&nbsp;:',
	'champ_sgbd_gestion_label' => 'URL du gestionnaire de SGBD&nbsp;:',
	'champ_sgbd_login_label' => 'Identifiant&nbsp;:',
	'champ_sgbd_nom_label' => 'Nom de la SGBD&nbsp;:',
	'champ_sgbd_password_label' => 'Mot de passe&nbsp;:',
	'champ_sgbd_port_label' => 'Port&nbsp;:',
	'champ_sgbd_prefixe_label' => 'Préfixe des tables&nbsp;:',
	'champ_sgbd_serveur_label' => 'Serveur SGBD&nbsp;:',
	'champ_sgbd_type_explication' => 'Exemples: MySQL, Oracle, Microsoft SQL Server, MongoDB, etc.',
	'champ_sgbd_type_label' => 'Type de SGBD&nbsp;:',
	'champ_sgbd_version_label' => 'Version de la SGBD&nbsp;:',
	'champ_sso_explication' => 'Méthode d’authentification unique',
	'champ_sso_label' => 'SSO&nbsp;:',
	'champ_statistiques_label' => 'Outils de statistiques&nbsp;:',
	'champ_titre_label' => 'Titre',
	'champ_type_site_label' => 'Type de site&nbsp;:',
	'champ_uniqid_explication' => 'Une clé d’identification permettant de récupérer la configuration du serveur du site enregistré.',
	'champ_uniqid_label' => 'Unique ID&nbsp;:',
	'champ_versioning_path_label' => 'Chemin du dépôt&nbsp;:',
	'champ_versioning_trac_label' => 'Trac du dépôt&nbsp;:',
	'champ_versioning_type_explication' => 'Exemples: SVN, CVS, Git, etc.',
	'champ_versioning_type_label' => 'Type de versioning&nbsp;:',
	'champ_webservice_explication' => 'URL pour un suivi de configuration du site.',
	'champ_webservice_label' => 'Url du Web Service&nbsp;:',
	'confirmer_maj_projets_site' => 'Veuillez confirmer la demande de mise à jour de ce site par le biais du webservice.',
	'confirmer_supprimer_projets_site' => 'Êtes-vous sûr de vouloir supprimer les informations relatives à ce site ?',
	'consultation_webservice' => 'Consultation du webservice',

	// F
	'fo_fieldset_label' => 'Front office',
	'fo_url_format' => 'L’url du Front Office doit impérativement commencer par <em>http://</em> ou <em>https://</em>',

	// I
	'icone_creer_projets_site' => 'Ajouter un site',
	'icone_modifier_projets_site' => 'Modifier ce site',
	'info_1_auteur_admin' => 'Un administrateur',
	'info_1_auteur_webmestre' => 'Un webmestre',
	'info_1_plugin' => 'Un plugin',
	'info_1_projets_site' => 'Un site',
	'info_aucun_projets_site' => 'Aucun site',
	'info_creer_projetssite_non_autorise' => 'Vous n’avez pas les droits suffisants pour créer un site de projet.',
	'info_modifier_projetssite_non_autorise' => 'Vous n’avez pas les droits suffisants pour modifier un site de projet.',
	'info_nb_auteurs_admin' => '@nb@ administrateurs',
	'info_nb_auteurs_webmestres' => '@nb@ webmestres',
	'info_nb_plugins' => '@nb@ plugins',
	'info_nb_projets_sites' => '@nb@ sites',
	'info_projets_sites_auteur' => 'Les sites de cet auteur',
	'info_projetssite_non_autorise' => 'Vous n’avez pas les droits suffisants pour cette action.',
	'info_supprimer_projetssite_non_autorise' => 'Vous n’avez pas les droits suffisants pour supprimer un site de projet.',
	'info_voir_projetssite_non_autorise' => 'Vous n’avez pas les droits suffisants pour voir un site de projet.',

	// L
	'logiciel_fieldset_label' => 'Logiciel',
	'logiciel_plugins_titre_explication' => '<h4>Règles de saisies</h4>',

	// M
	'maj_projets_site' => 'Mettre à jour le site',
	'maj_webservice_log_ko' => 'Le site de projet n°@id@ n’a pu être mis à jour par l’url @webservice@',
	'maj_webservice_log_ok' => 'Le site de projet n°@id@ a été mis à jour par l’url @webservice@',

	// P
	'php_fieldset_label' => 'PHP',
	'prefixe_label' => 'Préfixe',
	'projets_site_champs_label' => 'Champs de la table <em>spip_projets_sites</em>',

	// R
	'retirer_lien_projets_site' => 'Retirer ce site',
	'retirer_tous_liens_projets_sites' => 'Retirer tous les sites',

	// S
	'sas_fieldset_label' => 'Système d’accès sécurisé (SAS)',
	'sas_nom_label' => 'Nom&nbsp;:',
	'serveur_fieldset_label' => 'Serveur du site',
	'sgbd_fieldset_label' => 'Informations sur la SGBD',
	'site_orphelin' => 'Site sans projet(s)',
	'statut_label' => 'Statut',
	'supprimer_projets_site' => 'Supprimer ce site',
	'supprimer_projets_site_explication' => 'La suppression de ce site risque de rompre les liens entres objets.',

	// T
	'texte_ajouter_projets_site' => 'Ajouter un site',
	'texte_changer_statut_projets_site' => 'Ce site est&nbsp;:',
	'texte_creer_associer_projets_site' => 'Créer et associer un site',
	'texte_non_fonction_referencement' => 'Vous pouvez préférer ne pas utiliser cette fonction automatique, et indiquer vous-même les éléments concernant ce site…',
	'texte_referencement_automatique' => '<strong>Ajout automatisé par webservice</strong><br/>Vous pouvez importer les données de votre site par le biais de l’url de webservice si vous l’avez configuré sur ce dernier.',
	'texte_referencement_automatique_verifier' => 'Veuillez vérifier les informations fournies par <tt>@url@</tt> avant d’enregistrer.',
	'titre_langue_projets_site' => 'Langue de ce site',
	'titre_logo_projets_site' => 'Logo de ce site',
	'titre_objets_lies_projets_site' => 'Liés à ce site&nbsp;:',
	'titre_projets_site' => 'Site',
	'titre_projets_sites' => 'Sites des projets',
	'titre_projets_sites_rubrique' => 'Sites de la rubrique',
	'type_site_' => 'Environnement non-défini',
	'type_site_01local' => 'Environnement local',
	'type_site_01local_abbr' => 'Local',
	'type_site_01local_court' => 'Local',
	'type_site_02dev' => 'Environnement de développement',
	'type_site_02dev_abbr' => 'Dev',
	'type_site_02dev_court' => 'Développement',
	'type_site_03inte' => 'Environnement d’intégration',
	'type_site_03inte_abbr' => 'Inté',
	'type_site_03inte_court' => 'Intégration',
	'type_site_04test' => 'Environnement de tests',
	'type_site_04test_abbr' => 'Test',
	'type_site_04test_court' => 'Tests',
	'type_site_05rec' => 'Environnement de recettes',
	'type_site_05rec_abbr' => 'Rec',
	'type_site_05rec_court' => 'Recettes',
	'type_site_06prep' => 'Environnement de pré-production',
	'type_site_06prep_abbr' => 'Pré-prod',
	'type_site_06prep_court' => 'Pré-production',
	'type_site_07prod' => 'Environnement de production',
	'type_site_07prod_abbr' => 'Prod',
	'type_site_07prod_court' => 'Production',
	'type_site__abbr' => 'N/A',
	'type_site__court' => 'Non défini',
	'type_site_dev' => 'Environnement de développement',
	'type_site_dev_abbr' => 'Dev',
	'type_site_dev_court' => 'Développement',
	'type_site_prep' => 'Environnement de pré-production',
	'type_site_prep_abbr' => 'Pré-prod',
	'type_site_prep_court' => 'Pré-production',
	'type_site_prod' => 'Environnement de production',
	'type_site_prod_abbr' => 'Prod',
	'type_site_prod_court' => 'Production',
	'type_site_rec' => 'Environnement de recettes',
	'type_site_rec_abbr' => 'Rec',
	'type_site_rec_court' => 'Recettes',

	// V
	'version_base_label' => 'Version du schéma',
	'version_label' => 'Version',
	'versioning_fieldset_label' => 'Gestionnaire de versions',

	// W
	'webservice_absent' => 'Aucun webservice',
	'webservice_active_explication' => 'L’url de Webservice a été renseigné. De ce fait, certains champs sont cachés à la saisie car ceux-ci sont mis à jour par le biais du webservice.',
	'webservice_active_titre_explication' => 'Webservice actif',

);

