<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'projets_sites_description' => 'Ce plugin va vous permettre d\'ajouter à vos projets les infos sur vos sites en production, en recette et en développement.',
	'projets_sites_nom' => 'Sites pour projets',
	'projets_sites_slogan' => 'Ayez sous la main vos infos de sites en production, récette et développement',
);

